const assert = require('assert')
const { checkEnableTime } = require('../JobPosting')


// 3A
// Arrage
const startTime = new Date(2021, 1, 31);
const endTime = new Date(2021, 2, 5);
const today = new Date(2021, 2, 3);
const expectedResult = true;

// Action
const actualResult = checkEnableTime(startTime, endTime, today)

// Assert
describe('ทดสอบเวลาลงประกาศ', function () {
  it('Case : 1  { เวลาอยู่ระหว่าง เริ่มต้นและสิ้นสุด }', function () {
    assert.equal(actualResult, expectedResult);
  });;

  // 3A
  // Arrage
  const startTime2 = new Date(2021, 1, 31);
  const endTime2 = new Date(2021, 2, 5);
  const today2 = new Date(2021, 1, 31);
  const expectedResult2 = true;

  // Action
  const actualResult2 = checkEnableTime(startTime2, endTime2, today2)

  // Assert
  it('Case : 2  { เวลาเท่ากับ วันที่เริ่มต้น}', function () {
    assert.equal(actualResult2, expectedResult2);
  });

  // 3A
  // Arrage
  const startTime3 = new Date(2021, 1, 31);
  const endTime3 = new Date(2021, 2, 5);
  const today3 = new Date(2021, 2, 5);
  const expectedResult3 = true;

  // Action
  const actualResult3 = checkEnableTime(startTime3, endTime3, today3)

  // Assert
  it('Case : 3  { เวลาเท่ากับ วันที่สิ้นสุด }', function () {
    assert.equal(actualResult3, expectedResult3);
  });

  // 3A
  // Arrage
  const startTime4 = new Date(2021, 1, 31);
  const endTime4 = new Date(2021, 2, 5);
  const today4 = new Date(2021, 1, 20);
  const expectedResult4 = false;

  // Action
  const actualResult4 = checkEnableTime(startTime4, endTime4, today4)

  // Assert
  it('Case : 4  { เวลาน้อยกว่า วันที่เริ่มต้น }', function () {
    assert.equal(actualResult4, expectedResult4);
  });

  // 3A
  // Arrage
  const startTime5 = new Date(2021, 1, 31);
  const endTime5 = new Date(2021, 2, 5);
  const today5 = new Date(2021, 2, 6);
  const expectedResult5 = false;

  // Action
  const actualResult5 = checkEnableTime(startTime5, endTime5, today5)

  // Assert
  it('Case : 5  { เวลามากกว่า วันที่สิ้นสุด }', function () {
    assert.equal(actualResult5, expectedResult5);
  });
});